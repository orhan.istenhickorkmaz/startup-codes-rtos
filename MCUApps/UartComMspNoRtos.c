#include "msp.h"

int main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW |
            WDT_A_CTL_HOLD;

    P2->DIR |= 0x07;
    CS->KEY = CS_KEY_VAL;
    CS->CTL0 = 0;
    CS->CTL0 = CS_CTL0_DCORSEL_3;
    CS->CTL1 = CS_CTL1_SELA_2 |
            CS_CTL1_SELS_3 |
            CS_CTL1_SELM_3;
    CS->KEY = 0;

    P1->SEL0 |= 0x0C;

    EUSCI_A0->CTLW0 |= 0x0001;
    EUSCI_A0->CTLW0 = 0x0001 | 0x0080;
    UCA0BRW = 78;
    UCA0MCTLW = (2 << 4) | 0x0001;

    UCA0CTLW0 &= ~0x0001;
    UCA0IFG &= ~0x0001;
    UCA0IE |= 0x0001;

    __enable_irq();
    NVIC->ISER[0] = 1 << ((EUSCIA0_IRQn) & 31);

    __sleep();

    while(1){
        // No Operation.
    }
}

void EUSCIA0_IRQHandler(void)
{
    P2->OUT = (UCA0RXBUF == 'y') ? 0x07 : 0x00;
}

