#include <unistd.h>
#include <stdint.h>
#include <stddef.h>

#include <ti/drivers/GPIO.h>
#include <ti/sysbios/BIOS.h>
#include <ti/drivers/timer/GPTimerCC26xx.h>
#include <xdc/runtime/Types.h>
#include "Board.h"

void timerFxn(GPTimerCC26XX_Handle handle, GPTimerCC26XX_IntMask interruptMask){
    GPIO_toggle(Board_GPIO_LED0);
    GPIO_toggle(Board_GPIO_LED1);
}

void *mainThread(void *arg0)
{
    GPIO_init();
    GPTimerCC26XX_Handle timerHandle;
    GPTimerCC26XX_Params timerParams;
    GPTimerCC26XX_Params_init(&timerParams);
    timerParams.width = GPT_CONFIG_16BIT;
    timerParams.mode = GPT_MODE_PERIODIC_UP;
    timerParams.debugStallMode = GPTimerCC26XX_DEBUG_STALL_OFF;
    timerHandle = GPTimerCC26XX_open(Board_GPTIMER0A, &timerParams);
    Types_FreqHz freq;
    BIOS_getCpuFreq(&freq);
    GPTimerCC26XX_Value loadVal = (freq.lo)*4;
    GPTimerCC26XX_setLoadValue(timerHandle, loadVal);
    GPTimerCC26XX_registerInterrupt(timerHandle, timerFxn, GPT_INT_TIMEOUT);
    GPTimerCC26XX_start(timerHandle);

    GPIO_write(Board_GPIO_LED1, Board_GPIO_LED_OFF);
    GPIO_write(Board_GPIO_LED0, Board_GPIO_LED_ON);

    return (NULL);
}
