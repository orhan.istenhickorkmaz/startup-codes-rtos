#include <stdint.h>
#include <stddef.h>
#include <ti/drivers/GPIO.h>
#include <ti/drivers/UART.h>
#include "Board.h"

void *mainThread(void *arg0) // Main Thread.
{
    char input;
    UART_Handle uart;
    UART_Params uartParams;

    GPIO_init();
    UART_init();

    GPIO_write(Board_GPIO_LED0, Board_GPIO_LED_ON);
    GPIO_write(Board_GPIO_LED1, Board_GPIO_LED_ON);

    UART_Params_init(&uartParams);
    uartParams.readDataMode = UART_DATA_BINARY;
    uartParams.readReturnMode = UART_RETURN_FULL;
    uartParams.readEcho = UART_ECHO_OFF;
    uartParams.baudRate = 9600;

    uart = UART_open(Board_UART0, &uartParams);

    if (uart == NULL) {
        System_printf("Cannot connect UART.");
    }

    while (1) {
        UART_read(uart, &input, 1);
        switch(input){
            case '0': GPIO_toggle(Board_GPIO_LED0);
            case '1': GPIO_toggle(Board_GPIO_LED1);
            default: System_printf("Invalid option.");
        }
    }
}
