/*!
 * \brief 32 bit test for circular buffer.
 *        Tested on MSP432P401R MCU. 
 * 
 */


#include "msp.h"
#include "circbuf.h"

void main(void)
{
    /*
        Stop watchdog timer first, then add data to
        buffer afterwards to check if it is same.
    */
    WDTCTL = WDTPW | WDTHOLD;
    circular_buffer_t buffer;
    cb_initialize(&buffer);
    int demo = 255;
    cb_put_item(&buffer, &demo);
    int data = *((int *)cb_pop_from(&buffer));

    __sleep();
    while(1);
}
