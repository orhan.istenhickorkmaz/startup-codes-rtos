#include "circbuf.h"

void cb_initialize(circular_buffer_t * buffer, void * buffer_array, uint8_t capacity, size_t single_size){
  buffer->head = 0;
  buffer->tail = 0;
  buffer->total_items = 0;
  buffer->capacity = capacity;
  buffer->single_size = single_size;
  buffer->data = buffer_array;
}

bool cb_is_empty(circular_buffer_t * buffer){
  bool flag = (buffer->total_items == 0) ? 1 : 0;
  return flag;
}

void cb_put_item(circular_buffer_t * buffer, void * data){
  uintptr_t temp = (uintptr_t) buffer->data;
  memcpy((void *)(temp + (buffer->tail * buffer->single_size)), data, buffer->single_size);
  buffer->tail = (buffer->tail + 1) % (buffer->capacity);

  if(buffer->total_items < (buffer->capacity))
    buffer->total_items++;
  else {
    memcpy((void *)(temp + (buffer->head * buffer->single_size)), data, buffer->single_size);
    buffer->head = (buffer->head + 1) % (buffer->capacity);
  }
}

void* cb_pop_from(circular_buffer_t * buffer){
  if(!buffer->total_items)
    return NULL;

  uint8_t temp_index = buffer->head;
  buffer->head = (buffer->head + 1) % (buffer->capacity);
  buffer->total_items--;

  return buffer->data + temp_index;
}

int cb_moving_average(circular_buffer_t * buffer){
  int idx;
  int temp_sum = 0;
  for(idx=0; idx<buffer->total_items; idx++){
    temp_sum += *((int *) buffer->data + idx);
  }
  return temp_sum/buffer->total_items;
}
