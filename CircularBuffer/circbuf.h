/*!
 * \brief Implementation of circular buffer, overrides after full.
 * \author Orhan Istenhickorkmaz
 * \date July, 2017
 */


#ifndef CIRCBUF_H
#define CIRCBUF_H

/*!
 * If studio.h was not included define NULL already.
 */
#ifndef NULL
#define NULL 0
#endif

/*!
 * stdint for data types, string for memcpy, stdbool for bool return.
 */
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

/*!
 * circular_buffer_t main struct type.
 */
typedef struct {
  uint8_t head;
  uint8_t tail;
  uint8_t total_items;
  uint8_t capacity;
  size_t single_size;
  void * data;
} circular_buffer_t;

/*!
 * Initialize circular buffer with given static array, array capacity, and sizeof(array data).
 * \param buffer type of circular_buffer_t
 * \param buffer_array type of anything.
 * \param capacity type of uint8_t maximum number of array size.
 * \param single_size the size of a single element in user array.
 * \return nothing.
 */
void cb_initialize(circular_buffer_t * buffer, void * buffer_array, uint8_t capacity, size_t single_size);
/*!
 * Checks if it is empty.
 * \param buffer
 * \return boolean true or false.
 */
bool cb_is_empty(circular_buffer_t * buffer);
/*!
 * Puts an item to buffer.
 * \param buffer
 * \param data type of any requested.
 * \return nothing.
 */
void cb_put_item(circular_buffer_t * buffer, void * data);
/*!
 * Pop item from buffer.
 * \param buffer
 * \return void pointer which data can be recovered with casting.
 */
void* cb_pop_from(circular_buffer_t * buffer);
/*!
 * Calculate moving average of all elements in buffer.
 * \param buffer
 * \return Moving average.
 */
int cb_moving_average(circular_buffer_t * buffer);



#endif /* CIRCBUF_H */
