/*!
 * \brief Brief Description
 *        For circular buffer library, it consists of 8 bit MCU test code.
 * \author Orhan Istenhickorkmaz
 * \date July 2017.
 * Tested on Arduino Uno ATmega328 microcontroller.
 * License : MIT
*/

#include <stdint.h>
#include <stdbool.h>
#include "circbuf.h"

void setup() {
  /*
   * Serial communication started at 9600 baud rate.
   */
  Serial.begin(9600);

}

void loop() {
  circular_buffer_t buffer;
  cb_initialize(&buffer);
  /*
   * A float has been added to buffer and pop from buffer again to see if
   * it is same again.
   */
  float demo = 0.2;
  cb_put_item(&buffer, &demo);
  float data = *((float *)cb_pop_from(&buffer));

  Serial.println(data);
  /*
   * Print data to UART.
   */

}
