#include <stdio.h>
#include "circbuf.h"

int main(int argc, char const *argv[]) {
  circular_buffer_t buffer;
  char buffer_array[2];

  cb_initialize(&buffer, (void *)buffer_array, 2, sizeof(char));

  char d = 'x';
  char z = 'f';
  char x = 'g';
  cb_put_item(&buffer, &z);
  cb_put_item(&buffer, &d);
  cb_put_item(&buffer, &x);
  printf("%c - %c\n", buffer_array[0], buffer_array[1]);

  circular_buffer_t sec_buffer;
  int int_array[2];
  cb_initialize(&sec_buffer, (void *)int_array, 2, sizeof(int));
  int ax = 140;
  int ay = 950;
  cb_put_item(&sec_buffer, &ax);
  cb_put_item(&sec_buffer, &ay);
  int return_data = *((int *)cb_pop_from(&sec_buffer));
  printf("Return data %d\n", return_data);
  printf("%d - %d\n", int_array[0], int_array[1]);

  circular_buffer_t flt_buffer;
  float flt_array[2];
  cb_initialize(&flt_buffer, (void *)flt_array, 2, sizeof(float));
  float flt_data = 24.2;
  float flt_sec = 44.3;
  float flt_third = 66.5;
  cb_put_item(&flt_buffer, &flt_data);
  cb_put_item(&flt_buffer, &flt_sec);
  cb_put_item(&flt_buffer, &flt_third);
  printf("%.1f - %.1f\n", flt_array[0], flt_array[1]);

  return 0;
}
