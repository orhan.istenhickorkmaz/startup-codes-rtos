#include <stdio.h>

struct Parent {
  int age;
  char *name;
};

typedef struct Parent ParentClass;

struct Child {
  ParentClass parent;
  char *name;
};

typedef struct Child ChildClass;

int main(int argc, char const *argv[]) {
  ChildClass me;
  me.parent.age = 40;
  me.parent.name = "Mehmet";
  return 0;
}
