#include "enc.h"

static double privateData;

static int privateFunction(void){
  return privateData;
}

int publicFunction(void){
  return privateFunction();
}

double getData(void){
  return privateData;
}

void setData(double x){
  privateData = x;
}
