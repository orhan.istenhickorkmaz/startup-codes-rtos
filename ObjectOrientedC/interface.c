#include <stdio.h>

typedef struct Something sm;

struct Something {
  int data;
  int secondData;
  void (*funcPointer)(sm *);
};

void printFunction(sm * Instance){
  printf("%d\n", Instance->data);
}

int main(int argc, char const *argv[]) {
  sm Object;
  Object.data = 5;
  Object.secondData = 6;
  Object.funcPointer = printFunction;

  Object.funcPointer(&Object);
  return 0;
}
