#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main(int argc, char const *argv[]) {
  void * ptr;

  char cnum = 'c';
  int inum = 2;
  uint8_t uintnum = 30;

  ptr = &cnum;
  printf("%c\n", *((char *) ptr));

  ptr = &inum;
  printf("%d\n", *((char *) ptr));

  ptr = &uintnum;
  printf("%u\n", *((uint8_t *) ptr));

  return 0;
}
