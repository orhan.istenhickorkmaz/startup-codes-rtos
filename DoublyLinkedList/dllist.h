/*!
 * \brief Implementing of a doubly linked list which points both prev and next.
 * \author Orhan Istenhickorkmaz
 * \date July, 2017
 */


#ifndef DLLIST_H
#define DLLIST_H

#ifndef NULL
#define NULL 0
#endif

/*!
 * Node struct for holding data, prev and next node.
 */

typedef struct node {
  void * data;
  struct Node* previous;
  struct Node* next;
} node_t;

/*!
 * LinkedList struct for holding head and last elements.
 */
typedef struct linked_list {
  node_t *head;
  node_t *last;
} dll_t;

/*!
 * Initialize list. Makes head and last NULL.
 * \param list struct.
 */
void list_init(dll_t *list);
/*!
 * Insert at head.
 * \param list struct
 * \param node struct
 */
void insert_head(dll_t *list, node_t *node);
/*!
 * Insert at tail.
 * \param list struct
 * \param node struct
 */
void insert_tail(dll_t *list, node_t *node);
/*!
 * Insert node before given node.
 * \param list struct
 * \param after_node the node before one wants to insert.
 * \param node struct
 */
void insert_before(dll_t *list, node_t *after_node, node_t *node);
/*!
 * Insert after before given node.
 * \param list struct
 * \param before_node the node after one wants to insert.
 * \param node struct
 */
void insert_after(dll_t *list, node_t *before_node, node_t *node);
/*!
 * Delete node, it already checks if it is head or last.
 * \param list struct
 * \param node struct
 */
void delete_node(dll_t *list, node_t *node);
/*!
 * Get current size.
 * \param list struct
 * \return unsigned int
 */
unsigned int get_size(dll_t *list);


#endif
