#include "dllist.h"

void list_init(dll_t *list){
  list->head = NULL;
  list->last = NULL;
}

void insert_head(dll_t *list, node_t *node){
  if(list->head == NULL){
    node->next     = NULL;
    node->previous = NULL;
    list->head     = node;
    return;
  }
  list->head->previous = node;
  node->next     = list->head;
  node->previous = NULL;
  list->head     = node;
}

void insert_tail(dll_t *list, node_t *node){
  node_t *temp = list->head;
  if(list->head == NULL){
    list->head = node;
    return;
  }
  while(temp->next != NULL)
    temp = temp->next;
  temp->next = node;
  node->previous = temp;
}

unsigned int get_size(dll_t *list){
  unsigned int count = 0;
  node_t *temp = list->head;
  while(temp != NULL){
    count++;
    temp = temp->next;
  }
  return count;
}

void insert_before(dll_t *list, node_t *after_node, node_t *node){
  node_t *previous_node = after_node->previous;
  previous_node->next   = node;
  node->previous        = previous_node;
  after_node->previous  = node;
  node->next            = after_node;
}

void insert_after(dll_t *list, node_t *before_node, node_t *node){
  node_t *next_node   = before_node->next;
  before_node->next   = node;
  node->previous      = before_node;
  next_node->previous = node;
  node->next          = next_node;
}

void delete_node(dll_t *list, node_t *node){
  if(list->head == node){
    list->head = node->next;
    return;
  }

  if(list->last == node){
    list->last = node->previous;
    return;
  }


  node_t *previous_node   = node->previous;
  node_t *next_node       = node->next;
  previous_node->next     = next_node->previous;

}
