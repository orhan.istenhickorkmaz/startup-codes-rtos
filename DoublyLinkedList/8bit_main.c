#include <stdint.h>

#ifndef NULL
#define NULL 0
#endif

#define AVAILABLE_NODES 30
#include "dllist.h"

dll_t list;
dll_t *list_ptr = &list;
node_t node;
node_t *node_ptr = &node;

void setup() {
  Serial.begin(9600);

  list_init(list_ptr);

  int demo = 22;
  node_ptr->data = &demo;

  insert_head(list_ptr, node_ptr);
}

void loop() {
  Serial.println(list_ptr->head->data);
}
