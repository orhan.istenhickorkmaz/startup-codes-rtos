#include <stdio.h>
#include "dllist.h"

int main(int argc, char const *argv[]) {
  dll_t list;
  dll_t *list_ptr = &list;
  list_init(list_ptr);
  node_t node;
  node_t *node_ptr = &node;
  int demo = 22;
  node_ptr->data = &demo;
  node_ptr->next = NULL;
  node_ptr->previous = NULL;

  printf("%d\n", get_size(list_ptr));

  insert_head(list_ptr, node_ptr);

  printf("%d\n", *((int *) list_ptr->head->data));

  printf("%d\n", get_size(list_ptr));

  delete_node(list_ptr, node_ptr);

  printf("%d\n", get_size(list_ptr));

  return 0;
}
